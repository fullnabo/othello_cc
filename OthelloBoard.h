#pragma once

#include <unordered_map>

/*
 * ******************************************************************************
 * @file    : Othello.h                                                         *
 * @author  : Sofian Touhami <sofian.engineer@gmail.com>                        *
 * @date    : August 5, 2019                                                    *
 * @version : 0.905                                                             *
 * ******************************************************************************
 */

/****************************************************************************
 * @class : OthelloBoard                                                    *
 *                                                                          *
 * @brief : Represents a Othello board, with public interface allowing easy *
 *          GUI construction                                                *
 ****************************************************************************/
class OthelloBoard
{
public:
    class Cell;

public:
    static const short GRID_SIZE = 8  ;
    static const short MAX_TURNS = 60 ;

    enum Move : short {
        LEFT, RIGHT, UP, DOWN, LEFT_UP, RIGHT_UP, LEFT_DOWN, RIGHT_DOWN, ALL
    };

    typedef std::unordered_map<Move, unsigned short> CellPoints;

private:
    enum _Pawn : short { _WHITE = -1, _NO_COLOR, _BLACK };

public:
    CellPoints cellPoints   [GRID_SIZE][GRID_SIZE];
    bool       playableCells[GRID_SIZE][GRID_SIZE];

private:
    _Pawn  _pawn_matrix[GRID_SIZE][GRID_SIZE];

    _Pawn  _current_player ;
    short  _turn_count     ;
    bool   _is_terminated  ;

public:
    void ConsoleOut ();
    void Play       (Cell const& cell);

public:
    OthelloBoard();

private:
    void       _reset               ();
    _Pawn      _opposite_player     ();
    _Pawn      _matrix_get          (Cell const& cell);
    void       _matrix_set          (Cell const& cell, _Pawn value);
    short      _check_points        (Cell const& cell, Move m);
    void       _reverse             (Cell const& cell, CellPoints const& cp);
    CellPoints _compute_cell_points (Cell const& cell);
    bool       _new_turn            ();

public:
    /*****************
     * @class : Cell
     *
     * @brief :
     *****************/
    class Cell
    {
        friend short OthelloBoard::_check_points (Cell const& c, Move m);
        friend void  OthelloBoard::_reverse      (Cell const& c, CellPoints const& cp);

    private:
        enum _HorizontalMove : short { _MOVE_LEFT = -1, _NO_HMOVE, _MOVE_RIGHT };
        enum _VerticalMove   : short { _MOVE_UP   = -1, _NO_VMOVE, _MOVE_DOWN  };

    public:
        static const short MAX_INDEX = 8;

        enum Index : short {
            ZERO, ONE, TWO, THREE, FOUR, FIVE, SIX, SEVEN
        };

        Index line   ;
        Index column ;

        Cell(Index line, Index column);
        Cell(Cell const& cell);

    private:
        bool _move      (OthelloBoard::Move m);
        bool _in_bounds (short const& line, short const& column);

        std::pair<_HorizontalMove, _VerticalMove> _decompose_move(OthelloBoard::Move m);
    };
};
