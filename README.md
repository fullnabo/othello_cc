## Othello ~ Back end algorithms

### The data structures
- The board is represented with a squared eight sized matrix.  
- A cell can take only three states : {*WHITE* - *EMPTY* - *BLACK*} representing a pawn of the specified color, or an empty cell.  
  
### The initial state
- All the cells are put to *EMPTY* state, except for the following coordinates[line, column]:  
  [3,3] = [4,4] = *WHITE* and [3,4] = [4,3] = *BLACK*.  
- The *WHITE* player always begin.  
  
### The rules
- A player can play only next to an opposite player's pawn, with the restriction to mark at least one point.  
- To earn points, a player must catch opposite player's pawn between two of his own pawn, in lines, columns and/or diagonals,  
   relatively to the cell where he puts his pawn.  
- If the conditions are not fullfilled, the player passes his turn.  
- In case both the players pass, the game finished.  
- The winner is the one with the most of pawns of his color on the board.

---

## Othello ~ GUI algorithms

