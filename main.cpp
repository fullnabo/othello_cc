#include "OthelloBoard.h"

#include <iostream>

using CellPoints = OthelloBoard::CellPoints;
using Cell       = OthelloBoard::Cell;
using Move       = OthelloBoard::Move;

int main (void)
{
    OthelloBoard board;
    board.ConsoleOut();

    return 0;
}
