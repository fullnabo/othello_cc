#
# @author  Sofian Touhami <sofian.engineer@gmail.com>
# @date    August 6, 2019
# @version 0.905
# 
# This file is under MIT Licence
#

EXE     := othello_cc.exe
MODULES := board.o

.RECIPEPREFIX := >

othello_cc : main.cpp $(MODULES)
> g++ -o $(EXE) main.cpp $(MODULES)

board.o : OthelloBoard.h
> g++ -std=c++17 -o board.o -c OthelloBoard.cpp

.PHONY := clean
clean :
> rm -f $(MODULES) $(EXE)
