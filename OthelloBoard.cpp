#include "OthelloBoard.h"

#include <iostream>
#include <cstring>

/*
 * ================================================================================ *
 * class OthelloBoard::Cell                                                         *
 * PUBLIC:                                                                          *
 *     Index :enum                                                                  *
 *                                                                                  *
 *     #line   :Index                                                               *
 *     #column :Index                                                               *
 *                                                                                  *
 *     @Cell (line, column)                                                         *
 *     @Cell (cell)                                                                 *
 *                                                                                  *
 * PRIVATE:                                                                         *
 *     @_move           (move)         :bool                                        *
 *     @_in_bounds      (line, column) :bool                                        *
 *     @_decompose_move (move)         :pair                                        *
 * ================================================================================ *
 */
using Cell = OthelloBoard::Cell;

Cell::Cell(Index line, Index column)
    : line   (line)
    , column (column)
{}

Cell::Cell(Cell const& cell)
    : line   (cell.line)
    , column (cell.column)
{}

bool
Cell::_move(OthelloBoard::Move m)
{
    auto[hstep, vstep] = _decompose_move(m);
    short tmpline = line +vstep, tmpcolumn = column +hstep; // @suppress("Symbol is not resolved")

    if (! _in_bounds(tmpline, tmpcolumn))
        return false;

    line   = static_cast<Index> (tmpline)   ;
    column = static_cast<Index> (tmpcolumn) ;

    return true;
}

bool
Cell::_in_bounds(short const& line, short const& column)
{
    return 0 <= line && line < OthelloBoard::GRID_SIZE && 0 <= column && column < OthelloBoard::GRID_SIZE;
}

std::pair<Cell::_HorizontalMove, Cell::_VerticalMove>
Cell::_decompose_move(OthelloBoard::Move m)
{
    using Move = OthelloBoard::Move;

    _HorizontalMove hm = _NO_HMOVE;
    _VerticalMove   vm = _NO_VMOVE;

    switch (m)
    {
    case Move::LEFT:
    case Move::LEFT_UP:
    case Move::LEFT_DOWN:
        hm = _MOVE_LEFT;
        break;

    case Move::RIGHT:
    case Move::RIGHT_UP:
    case Move::RIGHT_DOWN:
        hm = _MOVE_RIGHT;
        break;

    default: ;
    }

    switch (m)
    {
    case Move::UP:
    case Move::LEFT_UP:
    case Move::RIGHT_UP:
        vm = _MOVE_UP;
        break;

    case Move::DOWN:
    case Move::LEFT_DOWN:
    case Move::RIGHT_DOWN:
        vm = _MOVE_DOWN;
        break;

    default: ;
    }

    return {hm, vm};
}

/*
================================================================================
class OthelloBoard
PUBLIC:
    Move       : enum
    CellPoints : unordered_map<Move, short>

    #SIZE
    #MAX_MOVE
    #MAX_TURN

    @OthelloBoard()

    @Print()
    @ComputeCellPoints(cell)

PRIVATE:
    @_reset           ()
    @_opposite_player (current_player)
    @_matrix_get      (cell)
    @_check_points    (cell, move)
================================================================================
 */

OthelloBoard::OthelloBoard()
{
    _reset();
}

void
OthelloBoard::ConsoleOut()
{
    for (short line = 0 ; line < GRID_SIZE ; line++) {
        for (short column = 0 ; column < GRID_SIZE ; column++) {
            printf("% i ", _pawn_matrix[line][column]);
        }
        std::cout << std::endl;
    }
}

void
OthelloBoard::Play(Cell const& c)
{

}

void
OthelloBoard::_reset()
{
    _current_player = _WHITE    ;
    _turn_count     = MAX_TURNS ;
    _is_terminated  = false     ;

    memset(_pawn_matrix, 0, sizeof(_pawn_matrix));
    _pawn_matrix[3][3] = _WHITE;
    _pawn_matrix[4][4] = _WHITE;
    _pawn_matrix[3][4] = _BLACK;
    _pawn_matrix[4][3] = _BLACK;
}

OthelloBoard::_Pawn
OthelloBoard::_opposite_player()
{
    switch (_current_player)
    {
    case _WHITE: return _BLACK;
    case _BLACK: return _WHITE;

    default: return _current_player;
    }
}

OthelloBoard::_Pawn
OthelloBoard::_matrix_get(Cell const& c)
{
    return _pawn_matrix[c.line][c.column];
}

void
OthelloBoard::_matrix_set(Cell const& c, _Pawn value)
{
    _pawn_matrix[c.line][c.column] = value;
}

short
OthelloBoard::_check_points (Cell const& c, Move m)
{
    Cell curcell  = c;
    _Pawn oplayer = _opposite_player();

    short points = 0;

    while (curcell._move(m)) {
        if ( oplayer != _matrix_get(curcell) ) {
            break;
        }
        points++;
    }

    if (_current_player == _matrix_get(curcell))
        return points;

    return 0;
}

OthelloBoard::CellPoints
OthelloBoard::_compute_cell_points(Cell const& c)
{
    if (_NO_COLOR != _matrix_get(c))
        return {};

    CellPoints points;
    points[ALL] = 0;

    for (short move = 0 ; move < ALL ; move++)
    {
    	Move movecast    = static_cast<Move>(move);
        points[movecast] = _check_points(c, movecast);
        points[ALL]      += points[movecast];
    }

    return points;
}

void
OthelloBoard::_reverse(Cell const& c, CellPoints const& cp)
{
    Cell       curcell = c;
    CellPoints tmpcp   = cp;
    for (auto&[move, points] : tmpcp)
    {
        while (points)
        {
            curcell._move(move);
            _matrix_set(curcell, _current_player);
            points--;
        }
    }
}

bool
OthelloBoard::_new_turn()
{
	_current_player = _opposite_player();

	for (short line = 0 ; line <= Cell::MAX_INDEX ; line++) {
		for (short column = 0 ; column <= Cell::MAX_INDEX ; column++) {
			Cell::Index linecast   = static_cast<Cell::Index>(line);
			Cell::Index columncast = static_cast<Cell::Index>(column);

			cellPoints   [line][column] = _compute_cell_points({linecast, columncast});
			playableCells[line][column] = 0 < cellPoints;
		}
	}

	return 0 < --_turn_count;
}
